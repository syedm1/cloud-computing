# Author:SD SHARUKH

import json
import logging
import re
import time
import os

import couchdb
import pandas as pd
import tweepy
from afinn import Afinn

# intialize logger
logger = logging.getLogger('SociealmediaAnalysis')
logging.basicConfig(level=logging.DEBUG)
logger.setLevel(logging.WARNING)

# Intialize DB settings

db_name = os.getenv("DB_NAME", "db_tweeter")
server_location = os.getenv('COUCH_URL', "http://localhost:5984/")

# API Authentication details
USER = "Sharukh"

# twitter's app credentials
consumer_key = "nQq0n6emrIgJp1HL58ygIgJjA"
consumer_secret = "nDCvPlPRdyisbqO5Qf4s3DhB4ldMJpeBIDowx01c2MbmDKFG8k"
access_key = "804136640293650432-bJXOJL0HhCsbZhlOTcyCWrMcXxVJCdd"
access_secret = "JpWAywXJnoHF2D7qqKXcH97b1nG4JH5oXfSt3u7f8EIW6"

# Coords for tweet filtering
MELBOURNE_COORDS = os.getenv('COORDS', [144.7455, -37.9101, 145.1826, -37.6534])
AUS_COORDS = [110.95, -54.83, 159.29, -9.19]
WORLD_COORDS = [-169.3, -56.1, 177.3, 75.2]

# bad word list
arrBad = [
    '2g1c', '2 girls 1 cup', 'acrotomophilia', 'anal', 'anilingus', 'anus', 'arsehole', 'ass', 'asshole',
    'assmunch', 'auto erotic', 'autoerotic', 'babeland', 'baby batter', 'ball gag', 'ball gravy', 'ball kicking',
    'ball licking', 'ball sack', 'ball sucking', 'bangbros', 'bareback', 'barely legal', 'barenaked', 'bastardo',
    'bastinado', 'bbw', 'bdsm', 'beaver cleaver', 'beaver lips', 'bestiality', 'bi curious', 'big black',
    'big breasts', 'big knockers', 'big tits', 'bimbos', 'birdlock', 'bitch', 'black cock', 'blonde action',
    'blonde on blonde action', 'blow j', 'blow your l', 'blue waffle', 'blumpkin', 'bollocks', 'bondage', 'boner',
    'boob', 'boobs', 'booty call', 'brown showers', 'brunette action', 'bukkake', 'bulldyke', 'bullet vibe',
    'bung hole', 'bunghole', 'busty', 'butt', 'buttcheeks', 'butthole', 'camel toe', 'camgirl', 'camslut',
    'camwhore', 'carpet muncher', 'carpetmuncher', 'chocolate rosebuds', 'circlejerk', 'cleveland steamer', 'clit',
    'clitoris', 'clover clamps', 'clusterfuck', 'cock', 'cocks', 'coprolagnia', 'coprophilia', 'cornhole', 'cum',
    'cumming', 'cunnilingus', 'cunt', 'darkie', 'date rape', 'daterape', 'deep throat', 'deepthroat', 'dick',
    'dildo', 'dirty pillows', 'dirty sanchez', 'dog style', 'doggie style', 'doggiestyle', 'doggy style',
    'doggystyle', 'dolcett', 'domination', 'dominatrix', 'dommes', 'donkey punch', 'double dong',
    'double penetration', 'dp action', 'eat my ass', 'ecchi', 'ejaculation', 'erotic', 'erotism', 'escort',
    'ethical slut', 'eunuch', 'faggot', 'fecal', 'felch', 'fellatio', 'feltch', 'female squirting', 'femdom',
    'figging', 'fingering', 'fisting', 'foot fetish', 'footjob', 'frotting', 'fuck', 'fucking', 'fuck buttons',
    'fudge packer', 'fudgepacker', 'futanari', 'g-spot', 'gang bang', 'gay sex', 'genitals', 'giant cock',
    'girl on', 'girl on top', 'girls gone wild', 'goatcx', 'goatse', 'gokkun', 'golden shower', 'goo girl',
    'goodpoop', 'goregasm', 'grope', 'group sex', 'guro', 'hand job', 'handjob', 'hard core', 'hardcore', 'hentai',
    'homoerotic', 'honkey', 'hooker', 'hot chick', 'how to kill', 'how to murder', 'huge fat', 'humping', 'incest',
    'intercourse', 'jack off', 'jail bait', 'jailbait', 'jerk off', 'jigaboo', 'jiggaboo', 'jiggerboo', 'jizz',
    'juggs', 'kike', 'kinbaku', 'kinkster', 'kinky', 'knobbing', 'leather restraint', 'leather straight jacket',
    'lemon party', 'lolita', 'lovemaking', 'make me come', 'male squirting', 'masturbate', 'menage a trois', 'milf',
    'missionary position', 'motherfucker', 'mound of venus', 'mr hands', 'muff diver', 'muffdiving', 'nambla',
    'nawashi', 'negro', 'neonazi', 'nig nog', 'nigga', 'nigger', 'nimphomania', 'nipple', 'nipples', 'nsfw images',
    'nude', 'nudity', 'nympho', 'nymphomania', 'octopussy', 'omorashi', 'one cup two girls', 'one guy one jar',
    'orgasm', 'orgy', 'paedophile', 'panties', 'panty', 'pedobear', 'pedophile', 'pegging', 'penis', 'phone sex',
    'piece of shit', 'piss pig', 'pissing', 'pisspig', 'playboy', 'pleasure chest', 'pole smoker', 'ponyplay',
    'poof', 'poop chute', 'poopchute', 'porn', 'porno', 'pornography', 'prince albert piercing', 'pthc', 'pubes',
    'pussy', 'queaf', 'raghead', 'raging boner', 'rape', 'raping', 'rapist', 'rectum', 'reverse cowgirl', 'rimjob',
    'rimming', 'rosy palm', 'rosy palm and her 5 sisters', 'rusty trombone', 's&m', 'sadism', 'scat', 'schlong',
    'scissoring', 'semen', 'sex', 'sexo', 'sexy', 'shaved beaver', 'shaved pussy', 'shemale', 'shibari', 'shit',
    'shota', 'shrimping', 'slanteye', 'slut', 'smut', 'snatch', 'snowballing', 'sodomize', 'sodomy', 'spic',
    'spooge', 'spread legs', 'strap on', 'strapon', 'strappado', 'strip club', 'style doggy', 'suck', 'sucks',
    'suicide girls', 'sultry women', 'swastika', 'swinger', 'tainted love', 'taste my', 'tea bagging', 'threesome',
    'throating', 'tied up', 'tight white', 'tit', 'tits', 'titties', 'titty', 'tongue in a', 'topless', 'tosser',
    'towelhead', 'tranny', 'tribadism', 'tub girl', 'tubgirl', 'tushy', 'twat', 'twink', 'twinkie',
    'two girls one cup', 'undressing', 'upskirt', 'urethra play', 'urophilia', 'vagina', 'venus mound', 'vibrator',
    'violet blue', 'violet wand', 'vorarephilia', 'voyeur', 'vulva', 'wank', 'wet dream', 'wetback', 'white power',
    'women rapping', 'wrapping men', 'wrinkled starfish', 'xx', 'xxx', 'yaoi', 'yellow showers', 'yiffy',
    'zoophilia',
]

bad_words_re = re.compile("|".join(arrBad))

# Intialize twitter API


try:
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
except:
    logger.error("Twiiter API authentication failure")

# DB connection function

try:
    couch = couchdb.Server(server_location)
    if db_name not in couch:
        couch.create(db_name)
    db = couch[db_name]

except Exception as ex:
    logger.error("DB Connection failed")


# check for vulgarity
def check_vulgarity(text):
    if bad_words_re.search(text):
        return "Yes"
    else:
        return "No"


# preprocessor tweet cleaning options


# preprocessor tweet cleaning level1
def clean_tweet(tweet):
    '''
    Utility function to clean the text in a tweet by removing
    links using regex.
    '''
    pattern2 = r"http\S+"
    try:
        result = re.sub(pattern2, "", tweet)
        return result
    except Exception as ex:
        logger.warning("cleaning failure")


# preprocessor tweet cleaning level2
def super_clean_tweet(self, tweet):
    '''
    Utility function to clean tweet text by removing links, special characters
    using simple regex statements.
    '''
    pattern1 = r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"
    try:
        result = re.sub(pattern1, "", tweet)
        return result
    except Exception as ex:
        logger.warning("cleaning failure")


# get sentiment score for given tweet

def get_sentiment_score(tweet, lang):
    try:
        clean_text = clean_tweet(tweet)
        afinn = Afinn(language=lang, emoticons=True)
        sentiment = afinn.score(clean_text)
        return sentiment

    except:
        try:
            sc = super_clean_tweet(tweet)
            afinn = Afinn(language=lang, emoticons=True)
            sentiment = afinn.score(sc)
            return sentiment
        except:
            logger.error("Error: Failed to get sentiment" + tweet)


# Get Twitter data stream to flow in this    for data analysis and data saving into CouchDB
# Custom stream listener fetches data according to the filters set and saves them to database
class CustomStreamListener(tweepy.StreamListener):
    # A listener handles tweets received from the stream.
    # This is a custom listener that store received tweets to FILE.
    def on_data(self, data):
        try:
            # converts to json format then saves in couchdb
            tweets_json = json.loads(data)
            doc_id = tweets_json["id_str"]
            tweet_lang = tweets_json["lang"]
            sentiment_score = get_sentiment_score(tweets_json["text"], tweet_lang)
            vulgarity = check_vulgarity(tweets_json["text"])

            if not tweets_json['retweeted'] and 'RT @' not in tweets_json['text']:
                if sentiment_score is None:
                    # if invalid language, topic and sentiment is not added to document
                    doc = {"_id": doc_id, "tweet_data": tweets_json, "sentiment": 'No sentiment found',
                           "vulgar": vulgarity}
                else:
                    doc = {"_id": doc_id, "tweet_data": tweets_json, "sentiment": sentiment_score, "vulgar": vulgarity}

                # saves the document to database
                db.save(doc)
                logger.info('added: ' + doc_id)
                print('added: ' + doc_id)
                return True
            else:
                print("retweet")
                pass

        except BaseException as e:
            logger.warning(e)
            time.sleep(5)
        except couchdb.http.ResourceConflict:
            # handle duplicates
            time.sleep(5)

        def on_status(self, status):
            logger.info(status.author.screen_name, status.created_at, status.text)


# Initiate Twitter streaming and save data to Database
def call_stream():
    try:
        streamingAPI = tweepy.streaming.Stream(auth, CustomStreamListener())
        # Add filter of melbourne location co-ordinates here
        streamingAPI.filter(locations=AUS_COORDS)  # locations = MELBOURNE_COORDS)   track=['russia'])

    except:
        logger.error("Streaming failed")


# Read data from DB view to keep track of live data in pandas dataframe
def fetch_db_data():
    try:
        rows = db.view('_all_docs', include_docs=True)
        data = [row['doc'] for row in rows]
        # use pandas dataframes for better data analytics maybe or use webservices
        df = pd.DataFrame(data)
        print(df)
    except:
        logger.error("DB data fetch failure")


# use call_stream() to stream tweets get sentiment and save to CouchDB -> remove
#  in below line to stream twitter data and save it into your database and '#' to this if u want to use fetch_db_data()
call_stream()

# use fetch_db_data() to view all tweets with sentiments saved in database -> remove # in below line to view db data and
# add '#'  to this if u want to use call_stream()
# fetch_db_data()
