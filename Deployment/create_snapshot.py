#Team 50
import nectar as nc


# Get all the volume IDs
volume_ids = []
current_volume = nc.ec2_conn.get_all_volumes()
for vol_id in current_volume:
	volume_ids.append(vol_id.id)

# Create snapshot for each corresponding volume
for v in volume_ids:
	snapshot_name = "snapshot" + " for " + v
	nc.ec2_conn.create_snapshot(v, snapshot_name)