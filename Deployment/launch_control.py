#Team 50

import nectar as nc
import sys, time

INIT = 0
SPINNING = "running"
virtual_machines = ["Zookeeper + Nimbus + Supervisor - High RAM + Standard Storage", 
					"RabbitMQ - Standard RAM and Storage",
					"CouchDB + Tweeter Harvester + Aurin Harvester - Good Storage + RAM",
					"Virtualization - Standard RAM and Storage"]
#virtual_machines = ["TEST_AUTO_4_ins"]
NUM = 0

def ins_num():
	global NUM
	NUM = int(sys.argv[1])
	if len(sys.argv) < 2:
	    print("Usage: {} (number of instances)".format(sys.argv[INIT]))
	    print("e.g. python {} 4")
	    print("4 instances will be created. [{}]".format(virtual_machines))
	    sys.exit(-1)
	elif len(sys.argv) > 4:
		print("Max 4 instances are allowed.\nPlease enter instance number 1 - 4.")

def ins_check():
#	global NUM
#	NUM = sys.argv[1]
	if sys.argv[3:]:
		if sys.argv[2] == "check" and sys.argv[3] == "image":
			list_images()
		if sys.argv[2] == "check" and sys.argv[3] == "security":
			security_groups()
		if sys.argv[2] == "check" and sys.argv[3] == "instance":
			list_instances()
		if sys.argv[2] == "check" and sys.argv[3] == "volume":
			volume_info()
		if sys.argv[2] == "check" and sys.argv[3] == "all":
			list_images()
			security_groups()
			list_instances()
			volume_info()
		if sys.argv[2] == "terminate":
			terminate_instance()
	if len(sys.argv) == 2:
		print 'Team 50 Deployment...'
		resource_optimization_launch(nc.FLAVOUR_LARGE, INIT, nc.VOLUME_SIZE*2)
		print("Quick launch instances for \n	[{}]\n	[{}]".format(virtual_machines[1], virtual_machines[3]))
		launch_instances(2, nc.FLAVOUR_SMALL, nc.VOLUME_SIZE)
		resource_optimization_launch(nc.FLAVOUR_MEDIUM, 2, nc.VOLUME_SIZE)
		#attach_volume(NUM)

	sys.exit(-1)

	if sys.argv[2:]:
		print("\nUsage: python {} number check 'image', 'security', 'instance', 'volume', 'all'.\n".format(sys.argv[0]))
		sys.exit(-1)
		

def launch_instances(ins, t, volume_size):
	print("\nCreating instance(s)...")
	for x in range(ins):
		reservation = create_instance(t)
		instance = reservation.instances[INIT]
		
		# Volume
		#nc.ec2_conn.create_volume(nc.VOLUME_SIZE, nc.AVAILABILITY_ZONE)
		
		print("New instance [{}] has been created.".format(instance.id))
		print("Waiting for instances to be ready.")
		while instance.state != SPINNING:
			print(". ")
			time.sleep(1)
			instance.update()

		#nc.ec2_conn.attach_volume(vol_req, reservation.id, nc.PATH)
	
		#instance.add_tag(instance)
		print("\nInstance(s) successfully launched!\n")
		vol_id = create_vol(volume_size)
		attach_volume(vol_id, instance.id)
		ins_info(instance)
		
def resource_optimization_launch(t, at, volume_size):
	print "\nCreating Instance..."
	print("\n[{}]".format(virtual_machines[at]))
	reservation = create_instance(t)
	instance = reservation.instances[INIT]

#	volume = nc.ec2_conn.create_volume(nc.VOLUME_SIZE, nc.AVAILABILITY_ZONE)

	print("New instance [{}] has been created.".format(instance.id))
	print("Waiting for [{}] to be ready.".format(virtual_machines[at]))
	while instance.state != SPINNING:
		print(". ")
		time.sleep(1)
		instance.update()
	print("\n[{}] successfully launched!\n".format(virtual_machines[at]))
	vol_id = create_vol(volume_size)
	attach_volume(vol_id, instance.id)



def list_images():
	print("\nGetting all images information from Nectar...")
	images = nc.ec2_conn.get_all_images()

	for img in images:
		print("Image ID: {}, Image Name: {}".format(img.id, img.name))
		if img.id == nc.IMAGE:
			name = img.name
		
	print("\n\nThis is [{}] what our team is going to use.".format(name))

def list_instances():
	print("\nListing all instances information from Nectar...")	
	reservations = nc.ec2_conn.get_all_reservations()

	print("\n[Reservation(s)]:")
	print("Index\tID\t\tInstance")
	for idx, res in enumerate(reservations):
		print("{}\t{}\t{}".format(idx, res.id, res.instances))

	print("\n[Instance(s) Information]:")
	for idx, res in enumerate(reservations, 1):
		print("\nID: {}\tIP: {}\tPlacement: {}".format(res.instances[INIT].id,
						res.instances[INIT].private_ip_address,
						res.instances[INIT].placement))
	print ''

def ins_info(i):
	print("Successfully created.")
	print("IP Address:	{}".format(i.private_ip_address))
	print("Availability Zone:	{}".format(i.placement))
	print("Key Pair:	{}\n".format(i.key_name))

def create_instance(ins_type):
	reservation = nc.ec2_conn.run_instances(
		nc.IMAGE,
		key_name = nc.KEY,
		instance_type = ins_type,
		placement = nc.AVAILABILITY_ZONE,
		#kernel_id, ramdisk_id
		security_groups = nc.SECURITY_GROUPS)
	return reservation

def terminate_instance():
	# get boto objects for all the instances you wish to kill 
	all_ins = nc.ec2_conn.get_all_instances()
	# iterate through all the matching instances and stop them for res in all_ins:
	print("Terminating total instances of : {}".format(len(all_ins)))
	for instance in all_ins:
		print "Killing %s" % (instance.id)
		instance.terminate()

def volume_info():
	print ("\n[Volume(s) Information]:")
	volumes = nc.ec2_conn.get_all_volumes()
	for m,n in enumerate(volumes):
	    print "%s \t %s \t %s \t %s \t %s \t %s" %(m+1,volumes[m].id,volumes[m].attach_data.instance_id,volumes[m].status,volumes[m].zone,volumes[m].size)

def security_groups():
	print("\n[Security Group(s)]:")
	for group in nc.ec2_conn.get_all_security_groups():
		print group.name

def attach_volume(ins):
	vol_req = []
	#Create volumes for instances, 3 volumes are 50GB, 1 volume is 100GB
	print "50GB - 3 volumes"
	#for x in range(ins - 1):
	#	nc.ec2_conn.create_volume(nc.VOLUME_SIZE, nc.AVAILABILITY_ZONE)

	print "100GB - 1 volume"
	#nc.ec2_conn.create_volume(nc.VOLUME_SIZE * 2, nc.AVAILABILITY_ZONE)
	vol_req = nc.ec2_conn.get_all_volumes()

	reservations = nc.ec2_conn.get_all_instances()
	#Attach the volumes to instances and mount them to /dev/vdc
	for x in range(ins):
	    nc.ec2_conn.attach_volume(vol_req[x], reservations.instance[x].id, nc.PATH)

	    volume_id=volumeList[i], instance_id=vmList[i], device='/dev/vdb'

	print("All Volumes have been successfully attached.\n")

def create_vol(size):
    vol = nc.ec2_conn.create_volume(size, nc.AVAILABILITY_ZONE)
    print('\nCreated {0}GB volume: {1}\n'.format(size, vol.id))
    return vol.id


def attach_volume(vol_id, i_id):
    vol = nc.ec2_conn.get_all_volumes([vol_id])[0]
    while vol.status != "available":
    	print '. '
    	time.sleep(1)
    	vol.update()
    nc.ec2_conn.attach_volume(vol.id, i_id, nc.PATH)
    print(' - attached to instance {0}'.format(i_id))

def main():
	ins_num()
	print("\nConnecting to Nectar Cloud...")
	ins_check()
	print("\nAll instances have been created and attached with corresponding volume size.")


if __name__ == '__main__':
	main()