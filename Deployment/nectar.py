#Team 50
#Config to access to nectarcloud

import boto
from boto.ec2.regioninfo import RegionInfo

KEY = "key"

FLAVOUR_LARGE = "m2.large"
#VCPUs	4
#Root Disk	30 GB
#Ephemeral Disk	80 GB
#Total Disk	110 GB
#RAM	12,288 MB
FLAVOUR_MEDIUM = "m2.medium"
#VCPUs	2
#Root Disk	30 GB
#Ephemeral Disk	0 GB
#Total Disk	30 GB
#RAM	6,144 MB
FLAVOUR_SMALL = "m2.small"
#VCPUs	1
#Root Disk	30 GB
#Ephemeral Disk	0 GB
#Total Disk	30 GB
#RAM	4,096 MB

IMAGE = "ami-00003837"

SECURITY_GROUPS = ["default"]

VOLUME_SIZE = 50 #GB
AVAILABILITY_ZONE = "melbourne-qh2"
PATH = "/dev/vdb"

region = RegionInfo(name = "melbourne", endpoint = "nova.rc.nectar.org.au")

ec2_conn = boto.connect_ec2(aws_access_key_id = "a25960808b464132b633e842686f31e6",
							aws_secret_access_key = "cb75e98afcaf49d3ab72ee1c797779bb",
							is_secure = True,
							region = region,
							port = 8773,
							path = "/services/Cloud",
							validate_certs = False)