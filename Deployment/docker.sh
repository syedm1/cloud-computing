docker run -p 5984:5984 --name couchdb -d couchdb

docker run -d --restart always --name zookeeper zookeeper

docker run -d --hostname my-rabbit --name rabbit byteflair/rabbitmq-stomp

docker run -d --restart always --name nimbus --link zookeeper:zookeeper --link couchdb:couchdb --link rabbit:rabbit storm storm nimbus

docker run -d --restart always --name supervisor --link zookeeper:zookeeper --link nimbus:nimbus --link couchdb:couchdb --link rabbit:rabbit storm storm supervisor

docker run -d -p 8080:8080 --restart always --name ui --link nimbus:nimbus storm storm ui

docker run --link nimbus:nimbus -it --rm -v c:/Users/dagrawal/Documents/WORKSPACE/poc-storm/target/poc-storm-1.0-SNAPSHOT-jar-with-dependencies.jar:/topology.jar storm storm jar /topology.jar au.dj.poc.storm.Application topology